package com.task.mihail.task2.presentation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyDetailsModel {

    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("companyName")
    @Expose
    private String companyName;

    @SerializedName("open")
    @Expose
    private double open;
    @SerializedName("close")
    @Expose
    private double close;
    @SerializedName("change")
    @Expose
    private double change;
    @SerializedName("extendedPrice")
    @Expose
    private double extendedPrice;

    @SerializedName("extendedChange")
    @Expose
    private double extendedChange;

    @SerializedName("extendedChangePercent")
    @Expose
    private double extendedChangePercent;

    @SerializedName("changePercent")
    @Expose
    private double changePercent;

    @SerializedName("latestSource")
    @Expose
    private String latestSource;

    @SerializedName("latestTime")
    @Expose
    private String latestTime;

    public String getLatestSource() {
        return latestSource;
    }

    public String getLatestTime() {
        return latestTime;
    }

    public double getChangePercent() {
        return changePercent;
    }

    public double getExtendedChangePercent() {
        return extendedChangePercent;
    }

    public double getExtendedChange() {
        return extendedChange;
    }

    public double getExtendedPrice() {
        return extendedPrice;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getCompanyName() {
        return companyName;
    }

    public double getOpen() {
        return open;
    }

    public double getClose() {
        return close;
    }

    public double getChange() {
        return change;
    }
}
