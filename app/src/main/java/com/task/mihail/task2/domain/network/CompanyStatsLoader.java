package com.task.mihail.task2.domain.network;

import android.util.Log;
import com.task.mihail.task2.data.network.NetworkService;
import com.task.mihail.task2.presentation.interfaces.OnEventLoadCompanyListener;
import com.task.mihail.task2.presentation.model.CompanyStatsModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyStatsLoader {

    public void loadCompanyStats(String symbol, final OnEventLoadCompanyListener<CompanyStatsModel> listener) {
        Call<CompanyStatsModel> call = NetworkService.getInstance().getJSONApi().getCompanyStats(symbol);
        call.enqueue(new Callback<CompanyStatsModel>() {

            @Override
            public void onResponse(Call<CompanyStatsModel> call, Response<CompanyStatsModel> response) {
                if (response.body() != null) {

                    CompanyStatsModel companyStatsModel = response.body();
                    listener.onEventLoad(companyStatsModel);
                } else {
                    listener.onEmptyLoad();
                    Log.i("task2/0", "onResponse: esponse.body() = null");
                }
            }

            @Override
            public void onFailure(Call<CompanyStatsModel> call, Throwable t) {
                listener.onFailureLoad();
                Log.i("task2/0", "An error occurred during networking");
            }
        });

    }
}
