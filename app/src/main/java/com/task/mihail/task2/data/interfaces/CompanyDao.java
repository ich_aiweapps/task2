package com.task.mihail.task2.data.interfaces;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import com.task.mihail.task2.presentation.model.CompanyModel;
import io.reactivex.Flowable;
import java.util.List;

@Dao
public interface CompanyDao {

//    @Query("SELECT * FROM company_list")
//    LiveData<List<CompanyModel>> getAllCompanyModel();

    @Query("SELECT * FROM company_list")
    Flowable<List<CompanyModel>> getAllCompanyModel();

    @Insert
    void insertCompanyModel(CompanyModel companyModel);

    @Query("DELETE FROM company_list")
    void deleteAll();


}
