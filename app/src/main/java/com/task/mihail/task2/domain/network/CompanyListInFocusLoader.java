package com.task.mihail.task2.domain.network;

import com.task.mihail.task2.data.network.NetworkService;
import com.task.mihail.task2.presentation.interfaces.OnEventLoadCompanyListener;
import com.task.mihail.task2.presentation.model.CompanyModel;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyListInFocusLoader {

    public void loadListInFocus(final OnEventLoadCompanyListener<List<CompanyModel>> listener) {

        Call<List<CompanyModel>> call = NetworkService.getInstance().getJSONApi().getCompanyListInFocus();
        call.enqueue(new Callback<List<CompanyModel>>() {

            @Override
            public void onResponse(Call<List<CompanyModel>> call, Response<List<CompanyModel>> response) {
                if (response.body() != null) {
                    List<CompanyModel> companyModelList = new ArrayList<>(response.body());
                    listener.onEventLoad(companyModelList);
                } else {
                    listener.onEmptyLoad();
                }
            }

            @Override
            public void onFailure(Call<List<CompanyModel>> call, Throwable t) {
                listener.onFailureLoad();
            }
        });
    }
}
