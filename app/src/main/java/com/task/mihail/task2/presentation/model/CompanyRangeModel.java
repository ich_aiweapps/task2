package com.task.mihail.task2.presentation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyRangeModel {

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("close")
    @Expose
    private double close;

    public String getDate() {
        return date;
    }

    public double getClose() {
        return close;
    }
}
