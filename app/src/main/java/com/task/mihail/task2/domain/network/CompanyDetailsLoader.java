package com.task.mihail.task2.domain.network;

import com.task.mihail.task2.data.network.NetworkService;
import com.task.mihail.task2.presentation.interfaces.OnEventLoadCompanyListener;
import com.task.mihail.task2.presentation.model.CompanyDetailsModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyDetailsLoader {

    public void loadCompanyDetails(String symbol, final OnEventLoadCompanyListener<CompanyDetailsModel> listener) {
        Call<CompanyDetailsModel> call = NetworkService.getInstance().getJSONApi().getCompanyDetails(symbol);
        call.enqueue(new Callback<CompanyDetailsModel>() {

            @Override
            public void onResponse(Call<CompanyDetailsModel> call, Response<CompanyDetailsModel> response) {
                if (response.body() != null) {
                    CompanyDetailsModel companyDetailsModel = response.body();
                    listener.onEventLoad(companyDetailsModel);
                } else {
                    listener.onEmptyLoad();
                }
            }

            @Override
            public void onFailure(Call<CompanyDetailsModel> call, Throwable t) {
                listener.onFailureLoad();
            }
        });

    }

}
