package com.task.mihail.task2.domain.network;

import android.support.annotation.NonNull;
import android.util.Log;
import com.task.mihail.task2.data.network.NetworkService;
import com.task.mihail.task2.presentation.interfaces.OnEventLoadCompanyListener;
import com.task.mihail.task2.presentation.model.CompanyRangeModel;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyRangeLoader {

    public void loadCompanyRange(String symbol, String range,
            final OnEventLoadCompanyListener<List<CompanyRangeModel>> listener) {
        Call<List<CompanyRangeModel>> call = NetworkService.getInstance().getJSONApi().getCompanyRange(symbol, range);
        call.enqueue(new Callback<List<CompanyRangeModel>>() {

            @Override
            public void onResponse(Call<List<CompanyRangeModel>> call,
                    @NonNull Response<List<CompanyRangeModel>> response) {
                if (response.body() != null) {
                    List<CompanyRangeModel> companyListRange = new ArrayList<>(response.body());
                    listener.onEventLoad(companyListRange);
                } else {
                    listener.onEmptyLoad();
                    Log.i("task2/0", "onResponse1231: esponse.body() = null");
                }
            }

            @Override
            public void onFailure(Call<List<CompanyRangeModel>> call, Throwable t) {
                listener.onFailureLoad();
                Log.i("task2/0", "123An error occurred during networking");
            }
        });

    }
}
