package com.task.mihail.task2.presentation.interfaces;

public interface OnEventLoadCompanyListener<T> {

    void onEventLoad(T t);

    void onFailureLoad();

    void onEmptyLoad();
}
