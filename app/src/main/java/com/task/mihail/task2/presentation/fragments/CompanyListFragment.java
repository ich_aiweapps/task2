package com.task.mihail.task2.presentation.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.task.mihail.task2.R;
import com.task.mihail.task2.data.network.MainViewModel;
import com.task.mihail.task2.domain.network.CompanyListInFocusLoader;
import com.task.mihail.task2.presentation.abstracts.EventLoadListener;
import com.task.mihail.task2.presentation.activities.MainActivity;
import com.task.mihail.task2.presentation.adapters.RecyclerAdapter;
import com.task.mihail.task2.presentation.interfaces.OnEventClickListener;
import com.task.mihail.task2.presentation.model.CompanyModel;
import com.yqritc.recyclerviewflexibledivider.FlexibleDividerDecoration.VisibilityProvider;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CompanyListFragment extends Fragment {

    private List<CompanyModel> companyModelList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private boolean hidden = true;
    private CompanyListInFocusLoader loader;
    private MainViewModel viewModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_list_in_focus, container, false);
        refreshLayout = view.findViewById(R.id.swipe_layout);
        loader = new CompanyListInFocusLoader();
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (isAdded() && getActivity() != null) {
                    loadList();
                }
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        getDataCompany();
        if (isAdded() && getActivity() != null && hidden) {
            loadList();
        }

        RecyclerAdapter adapter = new RecyclerAdapter(companyModelList, new OnEventClickListener() {
            @Override
            public void onEventClick(int position) {
                String s = companyModelList.get(position).getSymbol();
                ((MainActivity) getActivity()).openDetailScreen(s);
            }
        });

        recyclerView.setAdapter(adapter);

        recyclerView.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getContext())
                        .marginResId(R.dimen.leftmargin, R.dimen.rightmargin)
                        .visibilityProvider(new VisibilityProvider() {
                            @Override
                            public boolean shouldHideDivider(int position, RecyclerView parent) {
                                return false;
                            }
                        })
                        .build());
    }

    private void loadList() {
        loader.loadListInFocus(new EventLoadListener<List<CompanyModel>>(getContext()) {
            @Override
            public void onEventLoad(List<CompanyModel> companyModel) {
                Log.i("task2/0", "onEventLoad: ");
                companyModelList.clear();
                companyModelList.addAll(companyModel);
                recyclerView.getAdapter().notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
                addAllCompany();
            }
        });
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            this.hidden = hidden;
        }
    }

    private void getDataCompany() {
        Log.i("task2/0", "getDataCompany: ");
        Flowable<List<CompanyModel>> companyFromDB = viewModel.getCompany();

        companyFromDB.subscribeOn(Schedulers.io())
                .debounce(1000, TimeUnit.MILLISECONDS)
                .subscribe(new Consumer<List<CompanyModel>>() {
                    @Override
                    public void accept(List<CompanyModel> companyModels){
                        Log.i("task2/0", "accept: ");
                        try {
                            if (companyModels.size() ==0)
                            {companyModelList.clear();
                            companyModelList.addAll(companyModels);
                            recyclerView.getAdapter().notifyDataSetChanged();}
                        } catch (Exception e) {Log.i("task2/0", "Exception: " + e);}
                        //Only the original thread that created a view hierarchy can touch its views.
                    }
                });

    }

    private void addAllCompany() {
        viewModel.deleteCompany();
        for (CompanyModel model : companyModelList) {
            viewModel.insertCompany(model);
        }
    }

}
