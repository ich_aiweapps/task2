package com.task.mihail.task2.presentation.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "company_list")
public class CompanyModel implements Serializable {

    @PrimaryKey
    @NonNull
    @SerializedName("symbol")
    @Expose
    private String symbol;

    @SerializedName("companyName")
    @Expose
    private String companyName;

    @SerializedName("latestPrice")
    @Expose
    private double latestPrice;

    @SerializedName("change")
    @Expose
    private double change;

    @SerializedName("marketCap")
    @Expose
    private long marketCap;

    @SerializedName("ytdChange")
    @Expose
    private double ytdChange;

    @SerializedName("peRatio")
    @Expose
    private double peRatio;

    public CompanyModel(@NonNull String symbol, String companyName, double latestPrice, double change, long marketCap, double ytdChange, double peRatio) {
        this.symbol = symbol;
        this.companyName = companyName;
        this.latestPrice = latestPrice;
        this.change = change;
        this.marketCap = marketCap;
        this.ytdChange = ytdChange;
        this.peRatio = peRatio;
    }


    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public double getLatestPrice() {
        return latestPrice;
    }

    public void setLatestPrice(double latestPrice) {
        this.latestPrice = latestPrice;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public long getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(long marketCap) {
        this.marketCap = marketCap;
    }

    public double getYtdChange() {
        return ytdChange;
    }

    public void setYtdChange(double ytdChange) {
        this.ytdChange = ytdChange;
    }

    public double getPeRatio() {
        return peRatio;
    }

    public void setPeRatio(double peRatio) {
        this.peRatio = peRatio;
    }
}
