package com.task.mihail.task2.presentation.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.task.mihail.task2.R;
import com.task.mihail.task2.presentation.interfaces.OnEventClickListener;
import com.task.mihail.task2.presentation.model.CompanyModel;


public class ListInFocusViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    private OnEventClickListener listener;

    private TextView symbolView;
    private TextView companyNameView;
    private TextView priceView;
    private TextView changeView;
    private TextView marketCapView;
    private TextView ytdView;
    private TextView ratioView;

    public ListInFocusViewHolders(View itemView, OnEventClickListener listener) {
        super(itemView);
        symbolView = itemView.findViewById(R.id.symbol);
        companyNameView = itemView.findViewById(R.id.companyName);
        priceView = itemView.findViewById(R.id.price);
        changeView = itemView.findViewById(R.id.change);
        marketCapView = itemView.findViewById(R.id.marketCap);
        ytdView = itemView.findViewById(R.id.ytd);
        ratioView = itemView.findViewById(R.id.ratio);
        itemView.setOnClickListener(this);
        this.listener = listener;
    }

    public void bind(CompanyModel companyModel) {
        symbolView.setText(companyModel.getSymbol());
        companyNameView.setText(companyModel.getCompanyName());
        priceView.setText(String.valueOf(companyModel.getLatestPrice()));
        changeView.setText(String.valueOf(companyModel.getChange()));
        marketCapView.setText(String.valueOf(companyModel.getMarketCap()));
        ytdView.setText(String.valueOf(companyModel.getYtdChange()));
        ratioView.setText(String.valueOf(companyModel.getPeRatio()));

    }

    @Override
    public void onClick(View v) {
        listener.onEventClick(getAdapterPosition());
    }
}
