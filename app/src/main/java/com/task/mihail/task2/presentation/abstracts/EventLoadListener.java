package com.task.mihail.task2.presentation.abstracts;

import android.content.Context;
import android.widget.Toast;
import com.task.mihail.task2.presentation.interfaces.OnEventLoadCompanyListener;

public abstract class EventLoadListener<T> implements OnEventLoadCompanyListener<T> {

    Context context;

    public EventLoadListener(Context context) {
        this.context = context;
    }

    @Override
    public void onFailureLoad() {
        Toast.makeText(context, "Failed to load", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEmptyLoad() {
        Toast.makeText(context, "Load is empty", Toast.LENGTH_SHORT).show();
    }

}
