package com.task.mihail.task2.presentation.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.task.mihail.task2.R;


public class ProfileView extends LinearLayout {

    private TextView leftProfileDataTitleView;
    private TextView leftProfileDataView;

    private TextView rightProfileDataTitleView;
    private TextView rightProfileDataView;

    public ProfileView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation((LinearLayout.VERTICAL));
        LayoutInflater.from(context).inflate(R.layout.company_profile_model, this, true);
        leftProfileDataTitleView = findViewById(R.id.leftProfileDataTitle);
        leftProfileDataView = findViewById(R.id.leftProfileData);
        rightProfileDataTitleView = findViewById(R.id.rightProfileDataTitle);
        rightProfileDataView = findViewById(R.id.rightProfileData);
    }

    public void init(String one, String two, String three, String four) {
        leftProfileDataTitleView.setText(one);
        leftProfileDataView.setText(two);
        rightProfileDataTitleView.setText(three);
        rightProfileDataView.setText(four);
    }
}