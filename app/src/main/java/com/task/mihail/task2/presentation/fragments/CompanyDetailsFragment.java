package com.task.mihail.task2.presentation.fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Line;
import com.anychart.data.Set;
import com.anychart.enums.Anchor;
import com.anychart.enums.MarkerType;
import com.anychart.enums.TooltipPositionMode;
import com.anychart.graphics.vector.Stroke;
import com.task.mihail.task2.R;
import com.task.mihail.task2.presentation.abstracts.EventLoadListener;
import com.task.mihail.task2.presentation.model.CompanyDetailsModel;
import com.task.mihail.task2.presentation.model.CompanyProfileModel;
import com.task.mihail.task2.presentation.model.CompanyRangeModel;
import com.task.mihail.task2.presentation.model.CompanyStatsModel;
import com.task.mihail.task2.domain.network.CompanyDetailsLoader;
import com.task.mihail.task2.domain.network.CompanyProfileLoader;
import com.task.mihail.task2.domain.network.CompanyRangeLoader;
import com.task.mihail.task2.domain.network.CompanyStatsLoader;
import com.task.mihail.task2.presentation.view.ProfileView;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class CompanyDetailsFragment extends Fragment implements OnClickListener {

    private static final String FORMAT_CHANGE_PERCENT_POSITIVE = "(+%.2f%%)";
    private static final String FORMAT_CHANGE_PERCENT_POSITIVE_NO_PERCENT = "+%.2f";
    private static final String FORMAT_CHANGE_PERCENT_NEGATIVE = "(%.2f%%)";
    private static final String FORMAT_CHANGE_PERCENT_NEGATIVE_NO_PERCENT = "%.2f";
    private static final String FORMAT_CHANGE_PERCENT_DOUBLE_ACCURACY = "(+%.4f%%)";
    private static final String FORMAT_CHANGE_PERCENT_DOUBLE_ACCURACY_NEGATIVE = "(%.4f%%)";

    private String symbol;
    private AnyChartView chart;
    private TextView companyNameView;
    private TextView symbolView;
    private TextView closeView;
    private TextView changeView;
    private TextView changePercentView;
    private TextView extendedPriceView;
    private TextView extendedChangeView;
    private TextView extendedChangePercentView;
    private TextView lastTimeView;
    private TextView descriptionView;
    private TextView profileView;

    private ProfileView ExchangeEmlployeesProfileView;
    private ProfileView SectorIndustryProfileView;
    private ProfileView FloatSharesProfileView;
    private ProfileView WebsiteTagsProfileView;
    private ProgressBar progressBar;

    private String range;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        symbol = bundle.getString("symbolCompany");
        Toast.makeText(getContext(), symbol, Toast.LENGTH_SHORT).show();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_company_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        range = "5y";
        progressBar = view.findViewById(R.id.progressBar);

        chart = view.findViewById(R.id.lineChart);
        View closePriceDetailsPanel = view.findViewById(R.id.detailsModelClose);
        View ePriceDetailsPanel = view.findViewById(R.id.detailsModelEPrise);

        companyNameView = view.findViewById(R.id.dCompanyName);
        symbolView = view.findViewById(R.id.dSymbol);

        closeView = closePriceDetailsPanel.findViewById(R.id.closeOpenDetails);
        changeView = closePriceDetailsPanel.findViewById(R.id.changeDetails);
        changePercentView = closePriceDetailsPanel.findViewById(R.id.changePercentDetails);
        extendedPriceView = ePriceDetailsPanel.findViewById(R.id.closeOpenDetails);
        extendedChangeView = ePriceDetailsPanel.findViewById(R.id.changeDetails);
        extendedChangePercentView = ePriceDetailsPanel.findViewById(R.id.changePercentDetails);

        lastTimeView = view.findViewById(R.id.lastTime);
        descriptionView = view.findViewById(R.id.description);
        profileView = view.findViewById(R.id.profileView);

        ExchangeEmlployeesProfileView = view.findViewById(R.id.profileModel);
        SectorIndustryProfileView = view.findViewById(R.id.profileModel2);
        FloatSharesProfileView = view.findViewById(R.id.profileModel3);
        WebsiteTagsProfileView = view.findViewById(R.id.profileModel4);

        Button loadData5yButton = view.findViewById(R.id.loadData5y);
        Button loadData1yButton = view.findViewById(R.id.loadData1y);
        Button loadData6mButton = view.findViewById(R.id.loadData6m);
        Button loadData1mButton = view.findViewById(R.id.loadData1m);

        loadData5yButton.setOnClickListener(this);
        loadData1yButton.setOnClickListener(this);
        loadData6mButton.setOnClickListener(this);
        loadData1mButton.setOnClickListener(this);

        loadCompanyDetails();
        loadCompanyProfile();
        loadCompanyStats();
        loadCompanyRange();

    }

    private void loadCompanyRange() {
        CompanyRangeLoader loader = new CompanyRangeLoader();
        if (isAdded() && getActivity() != null) {
            loader.loadCompanyRange(symbol, range, new EventLoadListener<List<CompanyRangeModel>>(getContext()) {
                @Override
                public void onEventLoad(List<CompanyRangeModel> companyRangeModel) {

                    Cartesian cartesian = AnyChart.line();
                    cartesian.animation(true);

                    ArrayList<DataEntry> seriesData = new ArrayList<>();
                    for (CompanyRangeModel range : companyRangeModel) {
                        String g = range.getDate();
                        g = g.replaceAll("-", "");
                        int x = Integer.parseInt(g);
                        int y = (int) range.getClose();
                        seriesData.add(new ValueDataEntry(x, y));
                    }

                    setCortesian(cartesian);

                    Set set = Set.instantiate();
                    set.data(seriesData);

                    setLineChart(cartesian, set);

                    chart.setChart(cartesian);
                    chart.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                }

                void setCortesian(Cartesian cartesian) {
                    cartesian.padding(10d, 20d, 5d, 20d);
                    cartesian.crosshair().enabled(true);
                    cartesian.crosshair()
                            .yLabel(true)
                            .yStroke((Stroke) null, null, null, (String) null, (String) null);

                    cartesian.tooltip().positionMode(TooltipPositionMode.POINT);

                    cartesian.xAxis(0).labels().padding(5d, 5d, 5d, 5d);
                }

                void setLineChart(Cartesian cartesian, Set set) {

                    Line series1 = cartesian.line(set.mapAs("{ x: 'x', value: 'value' }"));
                    series1.name(symbol);
                    series1.hovered().markers().enabled(true);
                    series1.hovered().markers()
                            .type(MarkerType.CIRCLE)
                            .size(4d);
                    series1.tooltip()
                            .position("right")
                            .anchor(Anchor.LEFT_CENTER)
                            .offsetX(5d)
                            .offsetY(5d);

                    cartesian.legend().enabled(true);
                    cartesian.legend().fontSize(13d);
                    cartesian.legend().padding(0d, 0d, 10d, 0d);
                }

            });
        }
    }

    private void loadCompanyStats() {
        CompanyStatsLoader loader = new CompanyStatsLoader();
        if (isAdded() && getActivity() != null) {
            loader.loadCompanyStats(symbol, new EventLoadListener<CompanyStatsModel>(getContext()) {
                @Override
                public void onEventLoad(CompanyStatsModel companyStatsModel) {
                    if (isAdded() && getActivity() != null) {
                        FloatSharesProfileView.init(
                                getString(R.string.FLOAT), String.valueOf(
                                        new DecimalFormat("###,###.####").format(companyStatsModel.getFloatCompany()))
                                , getString(R.string.SHARES_OUTSTANDING), String.valueOf(
                                        new DecimalFormat("###,###.####")
                                                .format(companyStatsModel.getSharesOutstanding())));
                        if (companyStatsModel.getFloatCompany() == null
                                && companyStatsModel.getSharesOutstanding() == null) {
                            FloatSharesProfileView.setVisibility(View.INVISIBLE);
                        }
                    }
                }
            });
        }
    }

    private void loadCompanyProfile() {
        CompanyProfileLoader loader = new CompanyProfileLoader();

            loader.loadCompanyProfile(symbol, new EventLoadListener<CompanyProfileModel>(getContext()) {
                @Override
                public void onEventLoad(CompanyProfileModel companyProfileModel) {
                    if (isAdded() && getActivity() != null) {
                        descriptionView.setText(String.valueOf(companyProfileModel.getDescription()));
                        profileView.setText(R.string.profile);

                        ExchangeEmlployeesProfileView
                                .init(getString(R.string.EXCHANGE), String.valueOf(companyProfileModel.getExchange()),
                                        getString(R.string.NUMBER_OF_EMPLOYEES), "0");
                        SectorIndustryProfileView
                                .init(getString(R.string.SECTOR), String.valueOf(companyProfileModel.getSector()),
                                        getString(R.string.INDUSTRY), companyProfileModel.getIndustry());

                        StringBuilder builder = new StringBuilder();
                        for (int i = 0; i < companyProfileModel.getTags().size(); i++) {
                            builder.append(companyProfileModel.getTags().get(i) + "\n");
                        }
                        WebsiteTagsProfileView
                                .init(getString(R.string.WEBSITE), String.valueOf(companyProfileModel.getWebsite()),
                                        getString(R.string.TAGS), builder.toString());

                        if (companyProfileModel.getExchange() == null
                                && companyProfileModel.getNuberOfEmployees() == 0) {
                            ExchangeEmlployeesProfileView.setVisibility(View.INVISIBLE);
                        }
                        if (companyProfileModel.getSector() == null && companyProfileModel.getIndustry() == null) {
                            SectorIndustryProfileView.setVisibility(View.INVISIBLE);
                        }
                        if (companyProfileModel.getWebsite() == null && builder.toString().equals("")) {
                            WebsiteTagsProfileView.setVisibility(View.INVISIBLE);
                        }
                    }
                }

            });
    }

    private void loadCompanyDetails() {
        CompanyDetailsLoader loader = new CompanyDetailsLoader();
        if (isAdded() && getActivity() != null) {
            loader.loadCompanyDetails(symbol, new EventLoadListener<CompanyDetailsModel>(getContext()) {
                @SuppressLint("DefaultLocale")
                @Override
                public void onEventLoad(CompanyDetailsModel companyDetailsModel) {
                    companyNameView.setText(companyDetailsModel.getCompanyName());
                    symbolView.setText("(" + companyDetailsModel.getSymbol() + ")");
                    closeView.setText(String.valueOf(companyDetailsModel.getClose()));
                    if (companyDetailsModel.getChange() > 0) {
                        setText(changeView, FORMAT_CHANGE_PERCENT_POSITIVE_NO_PERCENT, companyDetailsModel,1);
                    } else {
                        setText(changeView, FORMAT_CHANGE_PERCENT_NEGATIVE_NO_PERCENT, companyDetailsModel,1);
                    }
                    setupColorBasedOnValue(changeView, companyDetailsModel.getChange());

                    if (companyDetailsModel.getChangePercent() > 0) {
                        setText(changePercentView, FORMAT_CHANGE_PERCENT_POSITIVE, companyDetailsModel,100);
                    } else {
                        setText(changePercentView, FORMAT_CHANGE_PERCENT_NEGATIVE, companyDetailsModel,100);
                    }
                    setupColorBasedOnValue(changePercentView, companyDetailsModel.getChangePercent());

                    extendedPriceView.setText(String.valueOf(companyDetailsModel.getExtendedPrice()));

                    if (companyDetailsModel.getExtendedChange() > 0) {
                        setText(extendedChangeView,FORMAT_CHANGE_PERCENT_POSITIVE_NO_PERCENT, companyDetailsModel,1);
                    } else {
                        setText(extendedChangeView,FORMAT_CHANGE_PERCENT_NEGATIVE_NO_PERCENT, companyDetailsModel,1);
                    }
                    setupColorBasedOnValue(extendedChangeView, companyDetailsModel.getExtendedChange());

                    if (companyDetailsModel.getExtendedChangePercent() > 0) {
                        setText(extendedChangePercentView,FORMAT_CHANGE_PERCENT_DOUBLE_ACCURACY, companyDetailsModel,1);
                    } else {
                        setText(extendedChangePercentView,FORMAT_CHANGE_PERCENT_DOUBLE_ACCURACY_NEGATIVE, companyDetailsModel,1);
                    }
                    setupColorBasedOnValue(extendedChangePercentView, companyDetailsModel.getExtendedChangePercent());

                    lastTimeView.setText(
                            companyDetailsModel.getLatestSource() + " as of " + companyDetailsModel.getLatestTime());

                }

                void setText(TextView textView, String constant,CompanyDetailsModel companyDetailsModel, int i) {
                    textView.setText(String.format(constant, companyDetailsModel.getChange()*i));
                }
                void setupColorBasedOnValue(TextView textView, double value) {

                    if (value > 0) {
                        textView.setTextColor(Color.GREEN);
                    } else {
                        textView.setTextColor(Color.RED);
                    }
                }

            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loadData5y:
                range = "5y";
                loadCompanyRange();
                break;
            case R.id.loadData1y:
                range = "1y";
                loadCompanyRange();
                break;
            case R.id.loadData6m:
                range = "6m";
                loadCompanyRange();
                break;
            case R.id.loadData1m:
                range = "1m";
                loadCompanyRange();
                break;

        }
    }
}
