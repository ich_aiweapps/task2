package com.task.mihail.task2.presentation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.math.BigInteger;

public class CompanyStatsModel {

    @SerializedName("float")
    @Expose
    private BigInteger floatCompany;

    @SerializedName("sharesOutstanding")
    @Expose
    private BigInteger sharesOutstanding;

    public BigInteger getSharesOutstanding() {
        return sharesOutstanding;
    }

    public BigInteger getFloatCompany() {
        return floatCompany;
    }
}
