package com.task.mihail.task2.presentation.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import com.task.mihail.task2.R;
import com.task.mihail.task2.presentation.fragments.CompanyDetailsFragment;
import com.task.mihail.task2.presentation.fragments.CompanyListFragment;

public class MainActivity extends AppCompatActivity {

    private static final String TAG_LIST = "TAG_LIST";
    private static final String TAG_DETAIL = "TAG_DETAIL";

    private FragmentTransaction fragmentTransaction;
    private String symbolCompany = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            CompanyListFragment companyListFragment = new CompanyListFragment();
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment, companyListFragment, TAG_LIST).commit();
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        }
    }

    public void openDetailScreen(String symbol) {
        CompanyDetailsFragment companyDetailsFragment = new CompanyDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("symbolCompany", symbol);
        companyDetailsFragment.setArguments(bundle);
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment, companyDetailsFragment, TAG_DETAIL).addToBackStack(null).commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("symbolCompany", symbolCompany);
    }
}


