package com.task.mihail.task2.presentation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class CompanyProfileModel {

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("exchange")
    @Expose
    private String exchange;

    @SerializedName("nuberOfEmployees")
    @Expose
    private int nuberOfEmployees;

    @SerializedName("industry")
    @Expose
    private String industry;

    @SerializedName("sector")
    @Expose
    private String sector;

    @SerializedName("website")
    @Expose
    private String website;

    @SerializedName("tags")
    @Expose
    private ArrayList<String> tags;

    public String getWebsite() {
        return website;
    }

    public String getSector() {
        return sector;
    }

    public String getIndustry() {
        return industry;
    }

    public int getNuberOfEmployees() {
        return nuberOfEmployees;
    }

    public String getExchange() {
        return exchange;
    }

    public String getDescription() {
        return description;
    }


    public ArrayList<String> getTags() {
        return tags;
    }
}
