package com.task.mihail.task2.domain.network;

import com.task.mihail.task2.data.network.NetworkService;
import com.task.mihail.task2.presentation.interfaces.OnEventLoadCompanyListener;
import com.task.mihail.task2.presentation.model.CompanyProfileModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyProfileLoader {

    public void loadCompanyProfile(String symbol, final OnEventLoadCompanyListener<CompanyProfileModel> listener) {
        Call<CompanyProfileModel> call = NetworkService.getInstance().getJSONApi().getCompanyProfile(symbol);
        call.enqueue(new Callback<CompanyProfileModel>() {

            @Override
            public void onResponse(Call<CompanyProfileModel> call, Response<CompanyProfileModel> response) {
                if (response.body() != null) {
                    CompanyProfileModel companyProfileModel = response.body();
                    listener.onEventLoad(companyProfileModel);
                } else {
                    listener.onEmptyLoad();
                }
            }

            @Override
            public void onFailure(Call<CompanyProfileModel> call, Throwable t) {
                listener.onFailureLoad();
            }
        });

    }
}
