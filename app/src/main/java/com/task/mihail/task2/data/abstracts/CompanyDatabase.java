package com.task.mihail.task2.data.abstracts;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import com.task.mihail.task2.data.interfaces.CompanyDao;
import com.task.mihail.task2.presentation.model.CompanyModel;

@Database(entities = {CompanyModel.class}, version = 1, exportSchema = false)
public abstract class CompanyDatabase extends RoomDatabase {

    private static final String DB_NAME = "company_database";
    public abstract CompanyDao companyDao();
    private static CompanyDatabase database;
    private static final Object LOCK = new Object();

    public static CompanyDatabase getInstance(final Context context) {
        synchronized (LOCK) {
            if (database == null) {
                database = Room.databaseBuilder(context.getApplicationContext(),
                        CompanyDatabase.class, DB_NAME)
                        .build();

            }
        }
        return database;
        }

}



//https://codelabs.developers.google.com/codelabs/android-room-with-a-view/#11

