package com.task.mihail.task2.presentation.interfaces;

public interface OnEventClickListener {

    void onEventClick(int position);
}

