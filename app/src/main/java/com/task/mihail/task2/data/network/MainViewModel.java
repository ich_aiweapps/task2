package com.task.mihail.task2.data.network;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.task.mihail.task2.data.abstracts.CompanyDatabase;
import com.task.mihail.task2.presentation.model.CompanyModel;

import io.reactivex.Flowable;
import java.util.List;

public class MainViewModel extends AndroidViewModel {

    private static CompanyDatabase database;
    private Flowable<List<CompanyModel>> company;


    public MainViewModel(@NonNull Application application) {
        super(application);
        database = CompanyDatabase.getInstance(getApplication());
        company = database.companyDao().getAllCompanyModel();
    }

    public Flowable<List<CompanyModel>> getCompany() {
        return company;
    }

    public void insertCompany(CompanyModel companyModel) {
        new InsertTask().execute(companyModel);
    }

    public void deleteCompany() {
        new DeleteTask().execute();
    }

    private static class InsertTask extends AsyncTask<CompanyModel, Void, Void> {
        @Override
        protected Void doInBackground(CompanyModel... companyModels) {
            database.companyDao().insertCompanyModel(companyModels[companyModels.length-1]);
            return null;
        }
    }

    private static class DeleteTask extends AsyncTask< List<CompanyModel>, Void, Void> {
        @Override
        protected Void doInBackground(List<CompanyModel>... lists) {
            database.companyDao().deleteAll();
            return null;
        }
    }

}
