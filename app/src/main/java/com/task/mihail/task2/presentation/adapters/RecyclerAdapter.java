package com.task.mihail.task2.presentation.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.task.mihail.task2.R;
import com.task.mihail.task2.presentation.interfaces.OnEventClickListener;
import com.task.mihail.task2.presentation.model.CompanyModel;
import com.task.mihail.task2.presentation.viewHolders.ListInFocusViewHolders;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<ListInFocusViewHolders> {

    private List<CompanyModel> companyModelList;
    private OnEventClickListener listener;

    public RecyclerAdapter(List<CompanyModel> companyModelList, OnEventClickListener listener) {
        this.companyModelList = companyModelList;
        this.listener = listener;
    }

    @NonNull
    @Override

    public ListInFocusViewHolders onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.company_model, viewGroup, false);
        return new ListInFocusViewHolders(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ListInFocusViewHolders ListInFocusViewHolders, int position) {
        CompanyModel companyModel = companyModelList.get(position);
        ListInFocusViewHolders.bind(companyModel);
    }

    @Override
    public int getItemCount() {
        return companyModelList.size();
    }


}
