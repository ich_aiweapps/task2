package com.task.mihail.task2.data.interfaces;

import com.task.mihail.task2.presentation.model.CompanyDetailsModel;
import com.task.mihail.task2.presentation.model.CompanyModel;
import com.task.mihail.task2.presentation.model.CompanyProfileModel;
import com.task.mihail.task2.presentation.model.CompanyRangeModel;
import com.task.mihail.task2.presentation.model.CompanyStatsModel;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CallApi {

    @GET("stock/market/collection/list?collectionName=in-focus")
    Call<List<CompanyModel>> getCompanyListInFocus();

    @GET("stock/{symbol}/quote")
    Call<CompanyDetailsModel> getCompanyDetails(@Path("symbol") String symbol);

    @GET("stock/{symbol}/company")
    Call<CompanyProfileModel> getCompanyProfile(@Path("symbol") String symbol);

    @GET("stock/{symbol}/stats")
    Call<CompanyStatsModel> getCompanyStats(@Path("symbol") String symbol);

    @GET("stock/{symbol}/chart/{range}")
    Call<List<CompanyRangeModel>> getCompanyRange(@Path("symbol") String symbol,
            @Path("range") String range);

}
